package ru.tinkoff.fintech.bank.core.service.valid;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationLoan;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.product.Loan;
import ru.tinkoff.fintech.bank.core.service.application.ApplicationService;
import ru.tinkoff.fintech.bank.core.service.product.ProductService;

import java.util.List;

@Service
public class CreditMoneyValidService implements ValidService<ApplicationLoan> {

    public static final Integer MAX_SUM_CREDITS = 1_000;

    private ApplicationService applicationService;
    private ProductService<Loan> creditMoneyProductService;

    public CreditMoneyValidService(ApplicationService applicationService, ProductService<Loan> creditMoneyProductService) {
        this.applicationService = applicationService;
        this.creditMoneyProductService = creditMoneyProductService;
    }

    @Override
    public boolean valid(ApplicationLoan productForm) {
        Integer customerId = applicationService.getById(productForm.getApplicationId()).getCustomerId();
        List<Loan> loanList = creditMoneyProductService.getProductsByCustomer(customerId);
        Integer limit = productForm.getLimit();
        for (Loan loan : loanList) {
            limit += loan.getLimit();
        }
        return limit < MAX_SUM_CREDITS;
    }

    @Override
    public TypeProduct getTypeProductValidate() {
        return TypeProduct.LOAN;
    }

}
