package ru.tinkoff.fintech.bank.core.domain.application;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public class ApplicationLoan extends AbstractApplication {

    private Integer limit;
    private Integer term;

    public ApplicationLoan() {
        typeProduct = TypeProduct.LOAN;
    }

    public ApplicationLoan(Integer idApp, TypeProduct typeProduct, Integer limit, Integer term) {
        super(idApp, typeProduct);
        this.limit = limit;
        this.term = term;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApplicationLoan)) return false;
        if (!super.equals(o)) return false;
        ApplicationLoan that = (ApplicationLoan) o;
        return Objects.equals(limit, that.limit) &&
                Objects.equals(term, that.term);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), limit, term);
    }

    @Override
    public String toString() {
        return "ApplicationLoan{" +
                "limit=" + limit +
                ", term=" + term +
                ", id=" + id +
                ", applicationId=" + applicationId +
                ", typeProduct=" + typeProduct +
                '}';
    }
}
