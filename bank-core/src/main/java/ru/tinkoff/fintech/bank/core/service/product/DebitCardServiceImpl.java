package ru.tinkoff.fintech.bank.core.service.product;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.domain.product.Card;
import ru.tinkoff.fintech.bank.core.repository.ProductRepository;
import ru.tinkoff.fintech.bank.core.utils.GeneratorSecretKey;

@Service
public class DebitCardServiceImpl extends AbstractProductService<Card, ProductRepository<Card>> {

    public static final Logger log = Logger.getLogger(DebitCardServiceImpl.class);

    private Mapper mapper = new DozerBeanMapper();
    private Integer number = 0;

    public DebitCardServiceImpl(ProductRepository<Card> repository) {
        super(repository);
    }

    @Override
    public Card create() {
        Card card = new Card();
        card.setSecretKey(GeneratorSecretKey.generateKey());
        card.setNumber(number++);
        card.setYear(0);
        card.setMouth(0);
        return card;
    }

    @Override
    public Card create(AbstractApplication abstractApplication) {
        Card card = create();
        mapper.map(abstractApplication, card);
        card.setId(null);
        return card;
    }

    @Override
    public TypeProduct getTypeProductService() {
        return TypeProduct.CARD;
    }
}
