package ru.tinkoff.fintech.bank.core.domain.product;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public class Card extends Product {

    private Integer number;
    private Integer year;
    private Integer mouth;
    private Integer secretKey;

    public Card() {
        typeProduct = TypeProduct.CARD;
    }

    public Card(Integer idCustomer, TypeProduct typeProduct, Integer number, Integer year, Integer mouth, Integer secretKey) {
        super(idCustomer, typeProduct);
        this.number = number;
        this.year = year;
        this.mouth = mouth;
        this.secretKey = secretKey;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMouth() {
        return mouth;
    }

    public void setMouth(Integer mouth) {
        this.mouth = mouth;
    }

    public Integer getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(Integer secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;
        if (!super.equals(o)) return false;
        Card card = (Card) o;
        return Objects.equals(number, card.number) &&
                Objects.equals(year, card.year) &&
                Objects.equals(mouth, card.mouth) &&
                Objects.equals(secretKey, card.secretKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), number, year, mouth, secretKey);
    }

    @Override
    public String toString() {
        return "Card{" +
                "number=" + number +
                ", year=" + year +
                ", mouth=" + mouth +
                ", secretKey=" + secretKey +
                ", id=" + id +
                ", customerId=" + customerId +
                ", typeProduct=" + typeProduct +
                '}';
    }
}
