package ru.tinkoff.fintech.bank.core.service.product;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.domain.product.Product;

import java.util.List;

public interface ProductService<P extends Product> {

    boolean checkProductById(Integer id);

    void remove(Integer id);

    P get(Integer id);

    Integer add(P product);

    List<P> getAll();

    List<P> getProductsByCustomer(Integer idCustomer);

    P create();

    P create(AbstractApplication abstractApplication);

    TypeProduct getTypeProductService();
}
