package ru.tinkoff.fintech.bank.core.service.auth;

import org.springframework.security.core.Authentication;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.exception.AuthorizationException;

import javax.servlet.http.HttpServletRequest;

public interface AuthService {

    Authentication getAuthentication(HttpServletRequest request) throws AuthorizationException;

    Integer getAuthCustomerId();

    String getAuthCustomerLogin();

    Customer getCustomerAuth();
}
