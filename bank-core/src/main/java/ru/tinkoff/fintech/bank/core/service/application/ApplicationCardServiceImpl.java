package ru.tinkoff.fintech.bank.core.service.application;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationCard;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.repository.ApplicationFormRepository;

@Service
public class ApplicationCardServiceImpl extends AbstractApplicationFormService<ApplicationCard, ApplicationFormRepository<ApplicationCard>> {

    public ApplicationCardServiceImpl(ApplicationFormRepository<ApplicationCard> repository, ApplicationService applicationService) {
        super(repository, applicationService);
    }

    @Override
    public boolean validCreate(ApplicationCard productForm) {
        return (productForm.getApplicationId() != null);
    }


    @Override
    public TypeProduct getTypeProductService() {
        return TypeProduct.CARD;
    }
}
