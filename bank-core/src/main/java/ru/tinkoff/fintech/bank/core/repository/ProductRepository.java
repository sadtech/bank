package ru.tinkoff.fintech.bank.core.repository;

import ru.tinkoff.fintech.bank.core.domain.product.Product;

import java.util.List;

public interface ProductRepository<T extends Product> {

    Integer add(T product);

    T get(Integer idProduct);

    void cleanById(Integer idProduct);

    List<T> getAll();
}
