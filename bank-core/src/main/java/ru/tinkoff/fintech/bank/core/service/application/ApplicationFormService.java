package ru.tinkoff.fintech.bank.core.service.application;

import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.exception.ApiException;

import java.util.List;

public interface ApplicationFormService<T extends AbstractApplication> {

    boolean checkProductById(Integer id);

    T create(T product) throws ApiException;

    void remove(Integer id);

    T get(Integer id);

    TypeProduct getTypeProductService();

    List<T> getAll();
}
