package ru.tinkoff.fintech.bank.core.service.product;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.product.Loan;
import ru.tinkoff.fintech.bank.core.repository.ProductRepository;

@Service
public class CreditMoneyServiceImpl extends AbstractProductService<Loan, ProductRepository<Loan>> {

    private Mapper mapper = new DozerBeanMapper();

    public CreditMoneyServiceImpl(ProductRepository<Loan> repository) {
        super(repository);
    }

    @Override
    public Loan create() {
        return new Loan();
    }

    @Override
    public Loan create(AbstractApplication abstractApplication) {
        Loan loan = create();
        mapper.map(abstractApplication, loan);
        loan.setId(null);
        return loan;
    }

    @Override
    public TypeProduct getTypeProductService() {
        return TypeProduct.LOAN;
    }
}
