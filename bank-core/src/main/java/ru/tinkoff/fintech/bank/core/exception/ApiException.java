package ru.tinkoff.fintech.bank.core.exception;

public class ApiException extends Exception {
    private final String type = "error";
    private final Integer code;
    private final String message;
    private final String description;

    public ApiException() {
        this.code = 0;
        this.message = "ApiException";
        this.description = "common mistake";
    }

    public ApiException(Integer code, String message, String description) {
        this.description = description;
        this.code = code;
        this.message = message;
    }

    public ApiException(Integer code, String message) {
        this(code, "Unknown", message);
    }

    public String getDescription() {
        return description;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
