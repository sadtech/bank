package ru.tinkoff.fintech.bank.core.service.valid;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;

public interface ValidService<P extends AbstractApplication> {

    boolean valid(P productForm);

    TypeProduct getTypeProductValidate();

}
