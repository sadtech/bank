package ru.tinkoff.fintech.bank.core.exception;

public class AuthorizationException extends ApiException {

    public AuthorizationException() {
        super(13, "Authorization failed");
    }
}
