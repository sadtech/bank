package ru.tinkoff.fintech.bank.core.repository;

import ru.tinkoff.fintech.bank.core.domain.Customer;

import java.util.Collection;

public interface CustomerRepository {

    Integer add(Customer customer);

    Customer getById(Integer personId);

    Customer getByLogin(String login);

    Collection<Customer> getAll();
}
