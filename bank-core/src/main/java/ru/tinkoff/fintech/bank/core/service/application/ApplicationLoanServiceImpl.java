package ru.tinkoff.fintech.bank.core.service.application;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationLoan;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.repository.ApplicationFormRepository;
import ru.tinkoff.fintech.bank.core.service.valid.CreditMoneyValidService;

@Service
public class ApplicationLoanServiceImpl extends AbstractApplicationFormService<ApplicationLoan, ApplicationFormRepository<ApplicationLoan>> {

    public static final Integer MAX_TERM_DAY_CREDIT = 365;
    public static final Integer MIN_TERM_DAY_CREDIT = 0;
    public static final Integer MIN_VALUE_CREDIT = 0;

    public ApplicationLoanServiceImpl(ApplicationFormRepository<ApplicationLoan> repository, ApplicationService applicationService) {
        super(repository, applicationService);
    }

    @Override
    public boolean validCreate(ApplicationLoan productForm) {
        return (productForm.getLimit() > MIN_VALUE_CREDIT && productForm.getLimit() < CreditMoneyValidService.MAX_SUM_CREDITS)
                && (productForm.getTerm() > MIN_TERM_DAY_CREDIT && productForm.getTerm() < MAX_TERM_DAY_CREDIT);
    }

    @Override
    public TypeProduct getTypeProductService() {
        return TypeProduct.LOAN;
    }
}
