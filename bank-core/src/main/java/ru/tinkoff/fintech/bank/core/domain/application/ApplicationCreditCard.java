package ru.tinkoff.fintech.bank.core.domain.application;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public class ApplicationCreditCard extends AbstractApplication {

    private Integer limit;

    public ApplicationCreditCard() {
        typeProduct = TypeProduct.CREDIT_CARD;
    }

    public ApplicationCreditCard(Integer idApp, TypeProduct typeProduct, Integer limit) {
        super(idApp, typeProduct);
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApplicationCreditCard)) return false;
        if (!super.equals(o)) return false;
        ApplicationCreditCard that = (ApplicationCreditCard) o;
        return Objects.equals(limit, that.limit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), limit);
    }

    @Override
    public String toString() {
        return "ApplicationCreditCard{" +
                "limit=" + limit +
                ", id=" + id +
                ", applicationId=" + applicationId +
                ", typeProduct=" + typeProduct +
                '}';
    }
}
