package ru.tinkoff.fintech.bank.core.domain.product;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public abstract class Product {

    protected Integer id;
    protected Integer customerId;
    protected TypeProduct typeProduct;

    public Product(Integer customerId, TypeProduct typeProduct) {
        this.customerId = customerId;
        this.typeProduct = typeProduct;
    }

    public Product() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(customerId, product.customerId) &&
                typeProduct == product.typeProduct;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerId, typeProduct);
    }


}
