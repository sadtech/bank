package ru.tinkoff.fintech.bank.core.domain.application;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public class FieldProductForm {

    private TypeProduct typeProduct;
    private Integer idProduct;

    public FieldProductForm(TypeProduct typeProduct, Integer idProduct) {
        this.typeProduct = typeProduct;
        this.idProduct = idProduct;
    }

    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldProductForm that = (FieldProductForm) o;
        return typeProduct == that.typeProduct &&
                Objects.equals(idProduct, that.idProduct);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeProduct, idProduct);
    }

    @Override
    public String toString() {
        return "FieldProductForm{" +
                "typeProduct=" + typeProduct +
                ", idProduct=" + idProduct +
                '}';
    }
}
