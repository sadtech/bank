package ru.tinkoff.fintech.bank.core.domain.product;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public class CreditCard extends Card {

    private Integer limit;

    public CreditCard() {
        typeProduct = TypeProduct.CREDIT_CARD;
    }

    public CreditCard(Integer idCustomer, TypeProduct typeProduct, Integer number, Integer year, Integer mouth, Integer secretKey, Integer limit) {
        super(idCustomer, typeProduct, number, year, mouth, secretKey);
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreditCard)) return false;
        if (!super.equals(o)) return false;
        CreditCard that = (CreditCard) o;
        return Objects.equals(limit, that.limit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), limit);
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "limit=" + limit +
                ", id=" + id +
                ", customerId=" + customerId +
                ", typeProduct=" + typeProduct +
                '}';
    }
}
