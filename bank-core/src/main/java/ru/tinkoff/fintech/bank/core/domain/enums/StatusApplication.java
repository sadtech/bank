package ru.tinkoff.fintech.bank.core.domain.enums;

public enum StatusApplication {

    DRAFT, NEW, APPROVED, REJECTED, CREATED

}
