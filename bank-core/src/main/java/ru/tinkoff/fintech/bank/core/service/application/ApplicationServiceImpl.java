package ru.tinkoff.fintech.bank.core.service.application;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.domain.application.Application;
import ru.tinkoff.fintech.bank.core.domain.application.FieldProductForm;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;
import ru.tinkoff.fintech.bank.core.domain.enums.StatusApplication;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.product.Product;
import ru.tinkoff.fintech.bank.core.repository.ApplicationRepository;
import ru.tinkoff.fintech.bank.core.service.CustomerService;
import ru.tinkoff.fintech.bank.core.service.product.ProductService;
import ru.tinkoff.fintech.bank.core.service.valid.ValidService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    public static final Logger log = Logger.getLogger(ApplicationServiceImpl.class);

    private final ApplicationRepository applicationRepository;

    private CustomerService customerService;
    private Map<TypeProduct, ApplicationFormService> productFormServiceMap;
    private Map<TypeProduct, ProductService> productServiceMap;
    private Map<TypeProduct, ValidService> validServiceMap;

    public ApplicationServiceImpl(ApplicationRepository applicationRepository, CustomerService customerService) {
        this.applicationRepository = applicationRepository;
        this.customerService = customerService;
    }
    

    @Autowired
    @Lazy
    public void setProductFormServiceMap(Map<TypeProduct, ApplicationFormService> productFormServiceMap) {
        this.productFormServiceMap = productFormServiceMap;
    }

    @Autowired
    @Lazy
    public void setProductServiceMap(Map<TypeProduct, ProductService> productServiceMap) {
        this.productServiceMap = productServiceMap;
    }

    @Autowired
    @Lazy
    public void setValidServiceMap(Map<TypeProduct, ValidService> validServiceMap) {
        this.validServiceMap = validServiceMap;
    }

    @Override
    public boolean checkAccess(Application application, String login) {
        Customer customer = customerService.getByLogin(login);
        return (customer.getRoles().contains(CustomerRole.ADMIN) || application.getCustomerId().equals(customer.getId()));
    }

    @Override
    public boolean forApproved(Application application) {
        if (application.getStatus().equals(StatusApplication.DRAFT) && application.getProduct() != null) {
            application.setStatus(StatusApplication.NEW);
            applicationRepository.edit(application);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean approved(Application application) {
        if (application.getStatus().equals(StatusApplication.NEW)) {
            application.setStatus(StatusApplication.APPROVED);
            applicationRepository.edit(application);
            return true;
        }
        return false;
    }

    @Override
    public boolean perform(Application application) {
        if (application.getStatus().equals(StatusApplication.APPROVED)) {
            AbstractApplication abstractApplication = productFormServiceMap.get(application.getProduct().getTypeProduct()).get(application.getProduct().getIdProduct());
            ProductService productService = productServiceMap.get(abstractApplication.getTypeProduct());
            if (!validServiceMap.containsKey(abstractApplication.getTypeProduct()) || validServiceMap.get(abstractApplication.getTypeProduct()).valid(abstractApplication)) {
                Product product = productService.create(abstractApplication);
                product.setCustomerId(application.getCustomerId());
                productService.add(product);
                application.setStatus(StatusApplication.CREATED);
                applicationRepository.edit(application);
                return true;
            } else {
                application.setStatus(StatusApplication.REJECTED);
                application.setMessage("Автоматическое отклонение");
            }
            applicationRepository.edit(application);
        }
        return false;
    }

    @Override
    public boolean refuse(Application application, String message) {
        if (application.getStatus().equals(StatusApplication.NEW)) {
            application.setStatus(StatusApplication.REJECTED);
            application.setMessage(message);
            applicationRepository.edit(application);
        }
        return true;
    }

    @Override
    public Application create(Integer idCustomer) {
        log.info("Создание новой заявки в репозитории");
        if (customerService.checkById(idCustomer)) {
            Application application = new Application();
            application.setStatus(StatusApplication.DRAFT);
            application.setCustomerId(idCustomer);
            Integer idCreate = applicationRepository.add(application);
            return applicationRepository.getById(idCreate);
        } else {
            return null;
        }
    }

    @Override
    public boolean addProductForm(Application application, AbstractApplication abstractApplication) {
        FieldProductForm oldFieldProductForm = application.getProduct();
        if (oldFieldProductForm != null) {
            productFormServiceMap.get(oldFieldProductForm.getTypeProduct()).remove(oldFieldProductForm.getIdProduct());
        }
        application.setProduct(new FieldProductForm(abstractApplication.getTypeProduct(), abstractApplication.getId()));
        applicationRepository.edit(application);
        return true;
    }

    @Override
    public List<Application> getAll() {
        return new ArrayList<>(applicationRepository.getAll());
    }

    @Override
    public Application getById(Integer id) {
        log.info("Запрошена заявка id: " + id);
        return applicationRepository.getById(id);
    }

    @Override
    public boolean existenceCheck(Integer idApp) {
        return applicationRepository.getById(idApp) != null;
    }

    @Override
    public List<Application> getAllByStatus(StatusApplication statusApplication) {
        log.info("Запрошены все заявки со статусом " + statusApplication.name());
        return applicationRepository.getAll().stream().filter(application -> application.getStatus().equals(statusApplication)).collect(Collectors.toList());
    }

    @Override
    public List<Application> getAllByCustomer(Integer idCustomer) {
        return new ArrayList<>(applicationRepository.getAllByCustomerId(idCustomer));
    }

}

