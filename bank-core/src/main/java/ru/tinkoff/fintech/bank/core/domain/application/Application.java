package ru.tinkoff.fintech.bank.core.domain.application;

import ru.tinkoff.fintech.bank.core.domain.enums.StatusApplication;

import java.util.Objects;

public class Application {

    private Integer id;
    private FieldProductForm product;
    private Integer customerId;
    private StatusApplication status;
    private String message;

    public Application() {

    }

    public Application(FieldProductForm product, Integer customerId, StatusApplication status, String message) {
        this.product = product;
        this.customerId = customerId;
        this.status = status;
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public StatusApplication getStatus() {
        return status;
    }

    public void setStatus(StatusApplication status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FieldProductForm getProduct() {
        return product;
    }

    public void setProduct(FieldProductForm product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Application that = (Application) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(product, that.product) &&
                Objects.equals(customerId, that.customerId) &&
                status == that.status &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, product, customerId, status, message);
    }

    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", product=" + product +
                ", customerId=" + customerId +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
