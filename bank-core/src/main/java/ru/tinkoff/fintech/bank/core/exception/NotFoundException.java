package ru.tinkoff.fintech.bank.core.exception;

public class NotFoundException extends ApiException {

    public NotFoundException() {
        super(2, "Not Found");
    }
}
