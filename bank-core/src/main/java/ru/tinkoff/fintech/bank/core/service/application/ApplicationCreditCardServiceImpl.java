package ru.tinkoff.fintech.bank.core.service.application;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationCreditCard;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.repository.ApplicationFormRepository;

@Service
public class ApplicationCreditCardServiceImpl extends AbstractApplicationFormService<ApplicationCreditCard, ApplicationFormRepository<ApplicationCreditCard>> {

    public ApplicationCreditCardServiceImpl(ApplicationFormRepository<ApplicationCreditCard> repository, ApplicationService applicationService) {
        super(repository, applicationService);
    }

    public boolean validCreate(ApplicationCreditCard applicationCreditCard) {
        return ((applicationCreditCard.getLimit() > 0 && applicationCreditCard.getLimit() < 1000)
                && applicationCreditCard.getApplicationId() != null);
    }

    @Override
    public TypeProduct getTypeProductService() {
        return TypeProduct.CREDIT_CARD;
    }
}
