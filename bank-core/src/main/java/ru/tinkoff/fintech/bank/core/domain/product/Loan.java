package ru.tinkoff.fintech.bank.core.domain.product;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public class Loan extends Product {

    private Integer limit;
    private Integer term;

    public Loan() {
        typeProduct = TypeProduct.LOAN;
    }

    public Loan(Integer idCustomer, TypeProduct typeProduct, Integer limit, Integer term) {
        super(idCustomer, typeProduct);
        this.limit = limit;
        this.term = term;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Loan)) return false;
        if (!super.equals(o)) return false;
        Loan that = (Loan) o;
        return Objects.equals(limit, that.limit) &&
                Objects.equals(term, that.term);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), limit, term);
    }

    @Override
    public String toString() {
        return "Loan{" +
                "limit=" + limit +
                ", term=" + term +
                ", id=" + id +
                ", customerId=" + customerId +
                ", typeProduct=" + typeProduct +
                '}';
    }
}
