package ru.tinkoff.fintech.bank.core.repository;

import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;

import java.util.Collection;

public interface ApplicationFormRepository<T extends AbstractApplication> {

    Integer add(T product);

    T get(Integer idProduct);

    void cleanById(Integer idProduct);

    Collection<T> getAll();
}
