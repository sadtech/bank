package ru.tinkoff.fintech.bank.core.service.application;

import ru.tinkoff.fintech.bank.core.domain.application.Application;
import ru.tinkoff.fintech.bank.core.domain.enums.StatusApplication;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;

import java.util.List;

public interface ApplicationService {

    Application create(Integer idCustomer);

    Application getById(Integer id);

    List<Application> getAll();

    List<Application> getAllByStatus(StatusApplication statusApplication);

    List<Application> getAllByCustomer(Integer idCustomer);

    boolean existenceCheck(Integer idApp);

    boolean checkAccess(Application application, String login);

    boolean forApproved(Application application);

    boolean approved(Application application);

    boolean perform(Application application);

    boolean refuse(Application application, String message);

    boolean addProductForm(Application application, AbstractApplication abstractApplication);
}
