package ru.tinkoff.fintech.bank.core.repository;

import ru.tinkoff.fintech.bank.core.domain.application.Application;

import java.util.Collection;

public interface ApplicationRepository {

    Integer add(Application application);

    Application getById(Integer id);

    Collection<Application> getAll();

    Collection<Application> getAllByCustomerId(Integer idCustomer);

    boolean edit(Application application);

}
