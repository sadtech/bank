package ru.tinkoff.fintech.bank.core.domain.application;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import java.util.Objects;

public abstract class AbstractApplication {

    protected Integer id;
    protected Integer applicationId;
    protected TypeProduct typeProduct;

    public AbstractApplication() {

    }

    public AbstractApplication(Integer applicationId, TypeProduct typeProduct) {
        this.applicationId = applicationId;
        this.typeProduct = typeProduct;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractApplication that = (AbstractApplication) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(applicationId, that.applicationId) &&
                typeProduct == that.typeProduct;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, applicationId, typeProduct);
    }
}
