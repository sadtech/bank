package ru.tinkoff.fintech.bank.core.service;

import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.exception.AuthorizationException;

import java.util.List;

public interface CustomerService {

    void add(Customer customer);

    boolean checkById(Integer personId);

    Integer getIdByLogin(String login);

    Customer getByLogin(String login);

    Customer getById(Integer idCustomer);

    List<Customer> getAll();

    boolean checkByLogin(String login);

    Customer registration(Customer customer) throws AuthorizationException;
}
