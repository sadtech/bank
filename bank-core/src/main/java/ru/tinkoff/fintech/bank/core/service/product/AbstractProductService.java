package ru.tinkoff.fintech.bank.core.service.product;

import ru.tinkoff.fintech.bank.core.domain.product.Product;
import ru.tinkoff.fintech.bank.core.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractProductService<P extends Product, R extends ProductRepository<P>> implements ProductService<P> {

    protected R repository;

    public AbstractProductService(R repository) {
        this.repository = repository;
    }

    @Override
    public boolean checkProductById(Integer id) {
        return repository.get(id) != null;
    }

    @Override
    public void remove(Integer id) {
        repository.cleanById(id);
    }

    @Override
    public P get(Integer id) {
        return repository.get(id);
    }

    @Override
    public Integer add(P product) {
        return repository.add(product);
    }

    @Override
    public List<P> getAll() {
        return repository.getAll();
    }

    @Override
    public List<P> getProductsByCustomer(Integer idCustomer) {
        return repository.getAll().stream().filter(product -> product.getCustomerId().equals(idCustomer)).collect(Collectors.toList());
    }

}
