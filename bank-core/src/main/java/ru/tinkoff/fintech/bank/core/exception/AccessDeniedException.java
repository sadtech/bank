package ru.tinkoff.fintech.bank.core.exception;

public class AccessDeniedException extends ApiException {

    public AccessDeniedException() {
        super(1, "Access denied");
    }

}
