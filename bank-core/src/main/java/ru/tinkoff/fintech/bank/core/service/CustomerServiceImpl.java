package ru.tinkoff.fintech.bank.core.service;

import org.apache.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;
import ru.tinkoff.fintech.bank.core.exception.AuthorizationException;
import ru.tinkoff.fintech.bank.core.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    public static final Logger log = Logger.getLogger(CustomerServiceImpl.class);

    private CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void add(Customer customer) {
        customerRepository.add(customer);
    }

    @Override
    public boolean checkById(Integer personId) {
        return customerRepository.getById(personId) != null;
    }

    @Override
    public Integer getIdByLogin(String login) {
        return customerRepository.getByLogin(login).getId();
    }

    @Override
    public Customer getByLogin(String login) {
        return customerRepository.getByLogin(login);
    }

    @Override
    public Customer getById(Integer idCustomer) {
        return customerRepository.getById(idCustomer);
    }

    @Override
    public List<Customer> getAll() {
        return new ArrayList<>(customerRepository.getAll());
    }

    @Override
    public boolean checkByLogin(String login) {
        return customerRepository.getByLogin(login) != null;
    }

    @Override
    public Customer registration(Customer customer) throws AuthorizationException {
        if (validRegistration(customer)) {
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            customer.setPassword(bCryptPasswordEncoder.encode(customer.getPassword()));
            customer.setRoles(Collections.singleton(CustomerRole.USER));
            Integer idCustomer = customerRepository.add(customer);
            return customerRepository.getById(idCustomer);
        } else {
            throw new AuthorizationException();
        }
    }

    private boolean validRegistration(Customer customer) {
        return !checkByLogin(customer.getUserName()) && customer.getFirstName() != null && customer.getPassword() != null;
    }
}
