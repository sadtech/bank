package ru.tinkoff.fintech.bank.core.domain.enums;

public enum CustomerRole {

    USER, ADMIN

}
