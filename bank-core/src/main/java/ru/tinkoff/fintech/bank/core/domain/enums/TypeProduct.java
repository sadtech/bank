package ru.tinkoff.fintech.bank.core.domain.enums;

public enum TypeProduct {

    CREDIT_CARD, LOAN, CARD

}
