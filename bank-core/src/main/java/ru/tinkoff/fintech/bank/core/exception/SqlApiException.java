package ru.tinkoff.fintech.bank.core.exception;

public class SqlApiException extends ApiException {

    public SqlApiException() {
        super(43, "Error sql constructor");
    }

    public SqlApiException(String description) {
        super(43, "Error sql constructor", description);
    }
}
