package ru.tinkoff.fintech.bank.core.utils;

import java.security.NoSuchAlgorithmException;
import java.util.Random;

import static java.security.SecureRandom.getInstanceStrong;

public class GeneratorSecretKey {

    private static final Integer MAX_VALUE_KEY = 1000;

    private GeneratorSecretKey() {
        throw new IllegalStateException("Utility Class");
    }

    public static Integer generateKey() {
        try {
            Random random = getInstanceStrong();
            return random.nextInt(MAX_VALUE_KEY);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
