package ru.tinkoff.fintech.bank.core.domain.application;

import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

public class ApplicationCard extends AbstractApplication {

    public ApplicationCard() {
        typeProduct = TypeProduct.CARD;
    }

    public ApplicationCard(Integer idApp, TypeProduct typeProduct) {
        super(idApp, typeProduct);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "ApplicationCard{" +
                "id=" + id +
                ", applicationId=" + applicationId +
                ", typeProduct=" + typeProduct +
                '}';
    }
}
