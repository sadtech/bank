package ru.tinkoff.fintech.bank.core.exception;

public class InvalidBodyException extends ApiException {

    public InvalidBodyException() {
        super(0, "Invalid request body");
    }

    public InvalidBodyException(String description) {
        super(0, "Invalid request body", description);
    }

}
