package ru.tinkoff.fintech.bank.core.service.product;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.domain.product.CreditCard;
import ru.tinkoff.fintech.bank.core.repository.ProductRepository;
import ru.tinkoff.fintech.bank.core.utils.GeneratorSecretKey;

@Service
public class CreditCardServiceImpl extends AbstractProductService<CreditCard, ProductRepository<CreditCard>> {

    public static final Logger log = Logger.getLogger(CreditCardServiceImpl.class);

    private Integer number = 0;
    private Mapper mapper = new DozerBeanMapper();

    public CreditCardServiceImpl(ProductRepository<CreditCard> repository) {
        super(repository);
    }

    @Override
    public CreditCard create() {
        CreditCard creditCard = new CreditCard();
        creditCard.setSecretKey(GeneratorSecretKey.generateKey());
        creditCard.setYear(0);
        creditCard.setMouth(0);
        creditCard.setNumber(number++);
        return creditCard;
    }

    @Override
    public CreditCard create(AbstractApplication abstractApplication) {
        CreditCard creditCard = create();
        mapper.map(abstractApplication, creditCard);
        creditCard.setId(null);
        return creditCard;
    }

    @Override
    public TypeProduct getTypeProductService() {
        return TypeProduct.CREDIT_CARD;
    }
}
