package ru.tinkoff.fintech.bank.core.service.application;

import ru.tinkoff.fintech.bank.core.domain.application.Application;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.exception.AccessDeniedException;
import ru.tinkoff.fintech.bank.core.exception.ApiException;
import ru.tinkoff.fintech.bank.core.exception.InvalidBodyException;
import ru.tinkoff.fintech.bank.core.repository.ApplicationFormRepository;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractApplicationFormService<P extends AbstractApplication, R extends ApplicationFormRepository<P>> implements ApplicationFormService<P> {

    protected final R repository;
    protected final ApplicationService applicationService;

    public AbstractApplicationFormService(R repository, ApplicationService applicationService) {
        this.repository = repository;
        this.applicationService = applicationService;
    }

    @Override
    public boolean checkProductById(Integer id) {
        return repository.get(id) != null;
    }

    @Override
    public P create(P productForm) throws ApiException {
        if (validCreate(productForm)) {
            Application application = applicationService.getById(productForm.getApplicationId());
            if (application != null) {
                Integer idCreate = repository.add(productForm);
                productForm = repository.get(idCreate);
                if (applicationService.addProductForm(application, productForm)) {
                    return productForm;
                } else {
                    throw new ApiException();
                }
            } else {
                throw new AccessDeniedException();
            }
        } else {
            throw new InvalidBodyException();
        }
    }

    public abstract boolean validCreate(P productForm);

    @Override
    public void remove(Integer id) {
        repository.cleanById(id);
    }

    @Override
    public P get(Integer id) {
        return repository.get(id);
    }

    @Override
    public List<P> getAll() {
        return new ArrayList<>(repository.getAll());
    }
}
