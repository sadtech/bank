package ru.tinkoff.fintech.bank.app.repository.application;

import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.application.AbstractApplication;
import ru.tinkoff.fintech.bank.core.repository.ApplicationFormRepository;

import java.util.Collection;

public abstract class AbstractApplicationFormRepositoryJDBC<P extends AbstractApplication> implements ApplicationFormRepository<P> {

    protected final JDBCService jdbcService;
    protected final String tableName;
    protected SQLInsert<P> sqlInsert;
    protected MapperJDBC<P> mapperJDBC;

    protected AbstractApplicationFormRepositoryJDBC(JDBCService jdbcService, String tableName) {
        this.jdbcService = jdbcService;
        this.tableName = tableName;
    }

    @Override
    public Integer add(P product) {
        return jdbcService.insertAndGetId(tableName, sqlInsert.getNameAndValues(product));
    }

    @Override
    public P get(Integer idProduct) {
        String sql = "SELECT * FROM " + tableName + " WHERE id = " + idProduct;
        return (P) jdbcService.getOneResult(sql, mapperJDBC);
    }

    @Override
    public void cleanById(Integer idProduct) {

    }

    @Override
    public Collection<P> getAll() {
        String sql = "SELECT * FROM " + tableName;
        return jdbcService.getResult(sql, mapperJDBC);
    }
}
