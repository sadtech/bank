package ru.tinkoff.fintech.bank.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"ru.tinkoff.fintech.bank.core.service", "ru.tinkoff.fintech.bank.app"})
public class SpringRunApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringRunApp.class);
    }

}
