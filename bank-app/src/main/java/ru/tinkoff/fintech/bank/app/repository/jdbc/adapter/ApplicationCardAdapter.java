package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationCard;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ApplicationCardAdapter implements SQLInsert<ApplicationCard>, MapperJDBC<ApplicationCard> {

    public static final Logger log = Logger.getLogger(ApplicationCardAdapter.class);

    @Override
    public Collection<ApplicationCard> getAll(ResultSet resultSet) {
        List<ApplicationCard> applicationLoans = new ArrayList<>();
        try {
            while (resultSet.next()) {
                ApplicationCard applicationCard = getApplicationCard(resultSet);
                applicationLoans.add(applicationCard);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return applicationLoans;
    }

    private ApplicationCard getApplicationCard(ResultSet resultSet) throws SQLException {
        ApplicationCard applicationCard = new ApplicationCard();
        applicationCard.setId(resultSet.getInt("id"));
        applicationCard.setApplicationId(resultSet.getInt("application_id"));
        return applicationCard;
    }

    @Override
    public ApplicationCard getOne(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return getApplicationCard(resultSet);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    @Override
    public Map<String, String> getNameAndValues(ApplicationCard applicationCard) {
        Map<String, String> map = new TreeMap<>();
        map.put("application_id", applicationCard.getApplicationId().toString());
        return map;
    }

    @Override
    public Set<List<String>> getValues(Set<ApplicationCard> objects) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (ApplicationCard object : objects) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }
}
