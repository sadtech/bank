package ru.tinkoff.fintech.bank.app.controller.admin;

import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.bank.core.domain.application.Application;
import ru.tinkoff.fintech.bank.core.domain.enums.StatusApplication;
import ru.tinkoff.fintech.bank.core.service.application.ApplicationService;

import java.util.List;

@RestController
@RequestMapping("api/v1/admin/application")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminApplicationController {

    private final ApplicationService applicationService;

    public AdminApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @GetMapping
    public ResponseEntity<List<Application>> getAllApplications() {
        return new ResponseEntity<>(applicationService.getAll(), HttpStatus.OK);
    }

    @PostMapping("{id}/refuse")
    public HttpStatus refuse(@PathVariable Integer id, @Param("message") String message) {
        Application application = applicationService.getById(id);
        if (applicationService.refuse(application, message)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.CONFLICT;
        }
    }

    @PostMapping("{id}/approved")
    public HttpStatus approved(@PathVariable Integer id) {
        Application application = applicationService.getById(id);
        if (applicationService.approved(application)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.CONFLICT;
        }
    }

    @GetMapping("/status/{status}")
    public ResponseEntity<List<Application>> getAllByStatus(@PathVariable String status) {
        return new ResponseEntity<>(applicationService.getAllByStatus(StatusApplication.valueOf(status.toUpperCase())), HttpStatus.OK);
    }

    @PostMapping("{id}/perform")
    public HttpStatus processingOnlyApproved(@PathVariable Integer id) {
        Application application = applicationService.getById(id);
        if (applicationService.perform(application)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.CONFLICT;
        }
    }


}
