package ru.tinkoff.fintech.bank.app.repository.product;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.CreditCardAdapter;
import ru.tinkoff.fintech.bank.core.domain.product.CreditCard;

@Service
public class CreditCardRepository extends AbstractProductRepositoryJDBC<CreditCard> {

    private final CreditCardAdapter creditCardAdapter = new CreditCardAdapter();

    public CreditCardRepository(JDBCService jdbcService) {
        super(jdbcService, "credit_card");
        super.mapperJDBC = creditCardAdapter;
        super.sqlInsert = creditCardAdapter;
    }

}
