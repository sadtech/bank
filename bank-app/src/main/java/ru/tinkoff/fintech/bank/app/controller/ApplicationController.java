package ru.tinkoff.fintech.bank.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.bank.core.domain.application.*;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.exception.AccessDeniedException;
import ru.tinkoff.fintech.bank.core.exception.ApiException;
import ru.tinkoff.fintech.bank.core.service.application.ApplicationFormService;
import ru.tinkoff.fintech.bank.core.service.application.ApplicationService;
import ru.tinkoff.fintech.bank.core.service.auth.AuthService;
import ru.tinkoff.fintech.bank.app.utils.ResponseEntityGson;

import java.util.Map;

@RestController
@RequestMapping("api/v1/application")
@PreAuthorize("hasAnyAuthority('USER')")
public class ApplicationController {

    private Map<TypeProduct, ApplicationFormService> applicationFormServiceMap;
    private final ApplicationService applicationService;
    private final AuthService authService;

    public ApplicationController(ApplicationService applicationService, AuthService authService) {
        this.applicationService = applicationService;
        this.authService = authService;
    }

    @Autowired
    public void setApplicationFormServiceMap(Map<TypeProduct, ApplicationFormService> applicationFormServiceMap) {
        this.applicationFormServiceMap = applicationFormServiceMap;
    }

    @PostMapping("create")
    public ResponseEntity<Application> create() {
        return new ResponseEntity<>(applicationService.create(authService.getAuthCustomerId()), HttpStatus.OK);
    }

    @GetMapping("{applicationId}")
    public ResponseEntity<String> getOne(@PathVariable Integer applicationId) {
        Application application = applicationService.getById(applicationId);
        if (applicationService.checkAccess(application, authService.getAuthCustomerLogin())) {
            return ResponseEntityGson.getJson(applicationService.getById(applicationId), HttpStatus.OK);
        } else {
            return ResponseEntityGson.getJson(new AccessDeniedException(), HttpStatus.LOCKED);
        }
    }

    @PostMapping("{id}/forApproval")
    public ResponseEntity<String> forApproval(@PathVariable Integer id) {
        Application application = applicationService.getById(id);
        if (applicationService.checkAccess(application, authService.getAuthCustomerLogin())) {
            if (applicationService.forApproved(application)) {
                return ResponseEntityGson.getJson(HttpStatus.OK);
            } else {
                return ResponseEntityGson.getJson(HttpStatus.CONFLICT);
            }
        } else {
            return ResponseEntityGson.getJson(new AccessDeniedException(), HttpStatus.LOCKED);
        }
    }

    @PostMapping("{applicationId}/product/card/credit/add")
    public ResponseEntity<String> addCreditCard(@PathVariable Integer applicationId, @RequestBody ApplicationCreditCard applicationCreditCard) {
        return createProductForm(applicationId, applicationCreditCard);
    }

    @PostMapping("{applicationId}/product/card/add")
    public ResponseEntity<String> addDebitCard(@PathVariable Integer applicationId, @RequestBody ApplicationCard applicationCard) {
        return createProductForm(applicationId, applicationCard);
    }

    @PostMapping("{applicationId}/product/loan/add")
    public ResponseEntity<String> addLoan(@PathVariable Integer applicationId, @RequestBody ApplicationLoan applicationLoan) {
        return createProductForm(applicationId, applicationLoan);
    }

    private ResponseEntity<String> createProductForm(Integer applicationId, AbstractApplication abstractApplication) {
        try {
            Application application = applicationService.getById(applicationId);
            if (applicationService.checkAccess(application, authService.getAuthCustomerLogin())) {
                abstractApplication.setApplicationId(applicationId);
                abstractApplication = applicationFormServiceMap.get(abstractApplication.getTypeProduct()).create(abstractApplication);
                return ResponseEntityGson.getJson(abstractApplication, HttpStatus.OK);
            } else {
                return ResponseEntityGson.getJson(new AccessDeniedException(), HttpStatus.LOCKED);
            }
        } catch (ApiException e) {
            return ResponseEntityGson.getJson(e, HttpStatus.BAD_REQUEST);
        }
    }


}
