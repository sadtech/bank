package ru.tinkoff.fintech.bank.app.repository.product;

import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.product.Product;
import ru.tinkoff.fintech.bank.core.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

public class AbstractProductRepositoryJDBC<P extends Product> implements ProductRepository<P> {

    protected final JDBCService jdbcService;
    protected final String tableName;
    protected SQLInsert<P> sqlInsert;
    protected MapperJDBC<P> mapperJDBC;

    public AbstractProductRepositoryJDBC(JDBCService jdbcService, String tableName) {
        this.jdbcService = jdbcService;
        this.tableName = tableName;
    }

    @Override
    public Integer add(P product) {
        return jdbcService.insertAndGetId(tableName, sqlInsert.getNameAndValues(product));
    }

    @Override
    public P get(Integer idProduct) {
        String sql = "SELECT * FROM " + tableName + " WHERE id = " + idProduct;
        return (P) jdbcService.getOneResult(sql, mapperJDBC);
    }

    @Override
    public void cleanById(Integer idProduct) {
        jdbcService.delete(tableName, "id = " + idProduct);
    }

    @Override
    public List<P> getAll() {
        String sql = "SELECT * FROM " + tableName;
        return new ArrayList<>(jdbcService.getResult(sql, mapperJDBC));
    }
}
