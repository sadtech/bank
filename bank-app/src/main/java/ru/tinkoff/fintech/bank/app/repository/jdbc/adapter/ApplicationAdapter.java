package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.application.Application;
import ru.tinkoff.fintech.bank.core.domain.application.FieldProductForm;
import ru.tinkoff.fintech.bank.core.domain.enums.StatusApplication;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;

import javax.validation.constraints.NotNull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ApplicationAdapter implements MapperJDBC<Application>, SQLInsert<Application> {

    public static final Logger log = Logger.getLogger(ApplicationAdapter.class);
    public static final String TYPE_PRODUCT = "type_product";
    public static final String PRODUCT_ID = "product_id";

    @Override
    public List<Application> getAll(@NotNull ResultSet resultSet) {
        List<Application> applications = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Application application = getApplication(resultSet);
                applications.add(application);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return applications;
    }

    private Application getApplication(@NotNull ResultSet resultSet) throws SQLException {
        Application application = new Application();
        application.setId(resultSet.getInt("id"));
        application.setStatus(StatusApplication.valueOf(resultSet.getString("status").toUpperCase()));
        application.setMessage(resultSet.getString("mess"));
        application.setCustomerId(resultSet.getInt("customer_id"));
        if (resultSet.getString(TYPE_PRODUCT) != null) {
            FieldProductForm fieldProductForm = new FieldProductForm(TypeProduct.valueOf(resultSet.getString(TYPE_PRODUCT).toUpperCase()), resultSet.getInt(PRODUCT_ID));
            application.setProduct(fieldProductForm);
        }
        return application;
    }


    @Override
    public Application getOne(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return getApplication(resultSet);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    @Override
    public Map<String, String> getNameAndValues(Application application) {
        Map<String, String> nameAndValue = new TreeMap<>();
        if (application.getId() != null) {
            nameAndValue.put("id", application.getId().toString());
        }
        nameAndValue.put("customer_id", (application.getCustomerId() != null) ? application.getCustomerId().toString() : null);
        if (application.getProduct() != null) {
            nameAndValue.put(TYPE_PRODUCT, "'" + application.getProduct().getTypeProduct().name().toLowerCase() + "'");
            nameAndValue.put(PRODUCT_ID, application.getProduct().getIdProduct().toString());
        } else {
            nameAndValue.put(TYPE_PRODUCT, null);
            nameAndValue.put(PRODUCT_ID, null);
        }
        nameAndValue.put("status", (application.getStatus() != null) ? "'" + application.getStatus().name().toLowerCase() + "'" : null);
        nameAndValue.put("mess", (application.getMessage() != null) ? "'" + application.getMessage() + "'" : null);
        return nameAndValue;
    }

    @Override
    public Set<List<String>> getValues(Set<Application> objects) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (Application object : objects) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }

}
