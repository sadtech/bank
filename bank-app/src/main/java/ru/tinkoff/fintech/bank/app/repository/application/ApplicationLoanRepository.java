package ru.tinkoff.fintech.bank.app.repository.application;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.ApplicationLoanAdapter;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationLoan;

@Service
public class ApplicationLoanRepository extends AbstractApplicationFormRepositoryJDBC<ApplicationLoan> {

    private final ApplicationLoanAdapter applicationLoanAdapter = new ApplicationLoanAdapter();

    protected ApplicationLoanRepository(JDBCService jdbcService) {
        super(jdbcService, "application_loan");
        super.sqlInsert = applicationLoanAdapter;
        super.mapperJDBC = applicationLoanAdapter;
    }
}
