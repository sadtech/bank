package ru.tinkoff.fintech.bank.app.repository.jdbc;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.exception.SqlApiException;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.*;

@Service
@PropertySource("classpath:/jdbc.properties")
public class PostgreSQLService implements JDBCService {

    public static final Logger log = Logger.getLogger(PostgreSQLService.class);
    public static final String INSERT_INTO_SQL = "INSERT INTO ";

    @Value("${db.url}")
    private String url;

    @Value("${db.username}")
    private String userName;

    @Value("${db.password}")
    private String password;

    @Override
    public Collection getResult(String sql, MapperJDBC mapperJDBC) {
        try (Connection conn = DriverManager.getConnection(url, userName, password); Statement statement = conn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            return mapperJDBC.getAll(resultSet);
        } catch (SQLException e) {
            log.error(e);
        }
        return Collections.emptyList();
    }

    @Override
    public Object getOneResult(String sql, MapperJDBC mapperJDBC) {
        try (Connection conn = DriverManager.getConnection(url, userName, password); Statement statement = conn.createStatement();) {
            ResultSet resultSet = statement.executeQuery(sql);
            return mapperJDBC.getOne(resultSet);
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    @Override
    public Integer insert(String tableName, Map<String, String> nameAndValue) {
        StringBuilder sql = new StringBuilder();
        Set<String> names = nameAndValue.keySet();
        try {
            sql.append(INSERT_INTO_SQL).append(tableName)
                    .append("(").append(getReduce(names.stream().reduce((x, y) -> x + ", " + y))).append(")")
                    .append(" VALUES")
                    .append("(").append(getReduce(nameAndValue.values().stream().reduce((x, y) -> x + ", " + y))).append(")");
        } catch (SqlApiException e) {
            log.error(e);
        }
        return execute(sql.toString());
    }

    @Override
    public Integer insertAndGetId(String tableName, Map<String, String> nameAndValue) {
        StringBuilder sql = new StringBuilder();
        Set<String> names = nameAndValue.keySet();

        try {
            sql.append(INSERT_INTO_SQL).append(tableName)
                    .append("(").append(getReduce(names.stream().reduce((x, y) -> x + ", " + y))).append(")")
                    .append(" VALUES")
                    .append("(").append(getReduce(nameAndValue.values().stream().reduce((x, y) -> x + ", " + y))).append(")");
        } catch (SqlApiException e) {
            log.error(e);
        }

        return executeAndGetId(sql.toString());
    }

    @Override
    public Integer insert(String tableName, List<String> fieldNames, Set<List<String>> valuesNameAndValue) {
        StringBuilder sql = new StringBuilder();
        try {
            sql.append(INSERT_INTO_SQL).append(tableName).append("(").append(getReduce(fieldNames.stream().reduce((x, y) -> x + ", " + y))).append(")").append("VALUES");
            String prefix = "";
            for (List<String> values : valuesNameAndValue) {
                sql.append(prefix).append("(").append(getReduce(values.stream().reduce((x, y) -> x + ", " + y))).append(")");
                prefix = ", ";
            }
        } catch (SqlApiException e) {
            log.error(e);
        }
        return execute(sql.toString());
    }


    private String getReduce(Optional<String> stringOptional) throws SqlApiException {
        if (stringOptional.isPresent()) {
            return stringOptional.get();
        } else {
            throw new SqlApiException();
        }
    }


    @Override
    public boolean update(@NotNull String tableName, @NotNull String whereRow, @NotNull Map<String, String> nameRowAndValue) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(tableName).append(" SET ");
        String prefix = "";
        for (Map.Entry<String, String> entry: nameRowAndValue.entrySet()) {
            String key = entry.getKey();
            if (!key.equals(whereRow)) {
                sql.append(prefix).append(key).append(" = ").append(nameRowAndValue.get(key));
                prefix = ", ";
            }
        }
        sql.append(" WHERE id = ").append(nameRowAndValue.get(whereRow));
        return execute(sql.toString()) > 0;
    }

    @Override
    public void delete(@NotNull String tableName, @NotNull String whereValue) {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE ").append(tableName).append("WHERE ").append(whereValue).append(";");
        execute(sql.toString());
    }

    private Integer execute(String sql) {
        try (Connection conn = DriverManager.getConnection(url, userName, password); Statement statement = conn.createStatement()) {
            return statement.executeUpdate(sql);
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    private Integer executeAndGetId(String sql) {
        try (Connection conn = DriverManager.getConnection(url, userName, password); PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            if (statement.executeUpdate() > 0) {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }
}
