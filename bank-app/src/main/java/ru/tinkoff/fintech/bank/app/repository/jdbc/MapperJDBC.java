package ru.tinkoff.fintech.bank.app.repository.jdbc;

import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;

import java.sql.ResultSet;
import java.util.Collection;

public interface MapperJDBC<T> extends SQLInsert<T> {

    Collection<T> getAll(ResultSet resultSet);

    T getOne(ResultSet resultSet);

}
