package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.InsertSQLForeignFey;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CustomerRoleAdapter implements InsertSQLForeignFey<Customer, Set<CustomerRole>> {

    @Override
    public Set<List<String>> getValues(Customer generalObject, Set<CustomerRole> foreignOject) {
        Set<List<String>> set = new HashSet<>();
        for (CustomerRole role : foreignOject) {
            List<String> list = new ArrayList<>();
            list.add(generalObject.getId().toString());
            list.add("'" + role.name().toLowerCase() + "'");
            set.add(list);
        }
        return set;
    }
}
