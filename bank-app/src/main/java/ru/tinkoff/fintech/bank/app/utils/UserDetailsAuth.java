package ru.tinkoff.fintech.bank.app.utils;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class UserDetailsAuth {

    private UserDetailsAuth() {
        throw new IllegalStateException();
    }

    public static UserDetails getProxy(Customer user) {
        InvocationHandler invocationHandler = new MyUserDetails(user);
        Class[] classes = new Class[]{UserDetails.class};
        return (UserDetails) Proxy.newProxyInstance(UserDetailsAuth.class.getClassLoader(), classes, invocationHandler);
    }

    static class MyUserDetails implements UserDetails, InvocationHandler {

        private transient Customer customer;

        MyUserDetails(Customer customer) {
            this.customer = customer;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            for (CustomerRole customerRole : customer.getRoles()) {
                switch (customerRole) {
                    case USER:
                        grantedAuthorities.add(Role.USER);
                        break;
                    case ADMIN:
                        grantedAuthorities.add(Role.ADMIN);
                }
            }
            return grantedAuthorities;
        }

        @Override
        public String getPassword() {
            return customer.getPassword();
        }

        @Override
        public String getUsername() {
            return customer.getUserName();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            return method.invoke(this, args);
        }

        enum Role implements GrantedAuthority {

            USER, ADMIN;

            @Override
            public String getAuthority() {
                return name();
            }
        }
    }
}

