package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.product.Loan;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class LoanAdapter implements SQLInsert<Loan>, MapperJDBC<Loan> {

    public static final Logger log = Logger.getLogger(LoanAdapter.class);

    @Override
    public Collection<Loan> getAll(ResultSet resultSet) {
        List<Loan> applicationLoans = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Loan loan = getLoan(resultSet);
                applicationLoans.add(loan);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return applicationLoans;
    }

    @Override
    public Loan getOne(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return getLoan(resultSet);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    private Loan getLoan(ResultSet resultSet) throws SQLException {
        Loan loan = new Loan();
        loan.setId(resultSet.getInt("id"));
        loan.setLimit(resultSet.getInt("limit_money"));
        loan.setTerm(resultSet.getInt("term_day"));
        loan.setCustomerId(resultSet.getInt("customer_id"));
        return loan;
    }

    @Override
    public Map<String, String> getNameAndValues(Loan loan) {
        Map<String, String> map = new TreeMap<>();
        map.put("customer_id", loan.getCustomerId().toString());
        map.put("limit_money", loan.getLimit().toString());
        map.put("term_day", loan.getTerm().toString());
        return map;
    }

    @Override
    public Set<List<String>> getValues(Set<Loan> objects) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (Loan object : objects) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }
}
