package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationCreditCard;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ApplicationCreditCardAdapter implements SQLInsert<ApplicationCreditCard>, MapperJDBC<ApplicationCreditCard> {

    public static final Logger log = Logger.getLogger(ApplicationCreditCardAdapter.class);

    @Override
    public Collection<ApplicationCreditCard> getAll(ResultSet resultSet) {
        List<ApplicationCreditCard> applicationLoans = new ArrayList<>();
        try {
            while (resultSet.next()) {
                ApplicationCreditCard applicationCreditCard = getApplicationCreditCard(resultSet);
                applicationLoans.add(applicationCreditCard);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return applicationLoans;
    }

    @Override
    public ApplicationCreditCard getOne(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return getApplicationCreditCard(resultSet);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    private ApplicationCreditCard getApplicationCreditCard(ResultSet resultSet) throws SQLException {
        ApplicationCreditCard applicationCreditCard = new ApplicationCreditCard();
        applicationCreditCard.setId(resultSet.getInt("id"));
        applicationCreditCard.setApplicationId(resultSet.getInt("application_id"));
        applicationCreditCard.setLimit(resultSet.getInt("limit_money"));
        return applicationCreditCard;
    }

    @Override
    public Map<String, String> getNameAndValues(ApplicationCreditCard applicationCreditCard) {
        Map<String, String> map = new TreeMap<>();
        map.put("application_id", applicationCreditCard.getApplicationId().toString());
        map.put("limit_money", applicationCreditCard.getLimit().toString());
        return map;
    }

    @Override
    public Set<List<String>> getValues(Set<ApplicationCreditCard> objects) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (ApplicationCreditCard object : objects) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }
}
