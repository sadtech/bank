package ru.tinkoff.fintech.bank.app.repository;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;
import ru.tinkoff.fintech.bank.core.repository.CustomerRepository;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.InsertSQLForeignFey;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.CustomerAdapter;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.CustomerRoleAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@PropertySource("classpath:/jdbc.properties")
public class CustomerRepositoryJDBC implements CustomerRepository {

    public static final Logger log = Logger.getLogger(CustomerRepositoryJDBC.class);

    private final JDBCService jdbcService;
    private final CustomerAdapter customerAdapter = new CustomerAdapter();
    private final InsertSQLForeignFey<Customer, Set<CustomerRole>> customerRoleInsert = new CustomerRoleAdapter();

    public CustomerRepositoryJDBC(JDBCService jdbcService) {
        this.jdbcService = jdbcService;
    }

    @Override
    public Integer add(Customer customer) {
        Integer idCustomer = jdbcService.insertAndGetId("customer", customerAdapter.getNameAndValues(customer));
        List<String> nameField = new ArrayList<>();
        nameField.add("customer_id");
        nameField.add("customer_role");
        Customer customerResult = getById(idCustomer);
        jdbcService.insert("customer_rols", nameField, customerRoleInsert.getValues(customerResult, customer.getRoles()));
        return idCustomer;
    }

    @Override
    public Customer getById(Integer customerId) {
        String sql = "SELECT id, customer_role, last_name, first_name, login, pass FROM customer LEFT JOIN customer_rols ON customer.id=customer_rols.customer_id WHERE customer.id=" + customerId;
        return (Customer) jdbcService.getOneResult(sql, customerAdapter);
    }

    @Override
    public Customer getByLogin(String login) {
        String sql = "SELECT id, customer_role, last_name, first_name, login, pass FROM customer LEFT JOIN customer_rols ON customer.id=customer_rols.customer_id WHERE customer.login='" + login + "'";
        return (Customer) jdbcService.getOneResult(sql, customerAdapter);

    }

    @Override
    public List<Customer> getAll() {
        String sql = "SELECT * FROM customer LEFT JOIN customer_rols ON customer.id=customer_rols.customer_id";
        return new ArrayList<>(jdbcService.getResult(sql, customerAdapter));
    }
}
