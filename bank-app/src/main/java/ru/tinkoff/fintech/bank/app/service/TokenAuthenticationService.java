package ru.tinkoff.fintech.bank.app.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.utils.UserDetailsAuth;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.exception.AuthorizationException;
import ru.tinkoff.fintech.bank.core.service.CustomerService;
import ru.tinkoff.fintech.bank.core.service.auth.AuthService;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
public class TokenAuthenticationService implements AuthService {

    private CustomerService customerService;

    public TokenAuthenticationService(CustomerService customerService) {
        this.customerService = customerService;
    }

    private static final long EXPIRATION_TIME = 864_000_000; // 10 days

    private static final String SECRET = "ThisIsASecret";

    private static final String TOKEN_PREFIX = "Bearer";

    private static final String HEADER_STRING = "Authorization";

    public static String addAuthentication(String username) {
        return Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();
    }

    @Override
    public Authentication getAuthentication(HttpServletRequest request) throws AuthorizationException {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            String user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody().getSubject();
            if (user != null && customerService.checkByLogin(user)) {
                return new UsernamePasswordAuthenticationToken(user, null, UserDetailsAuth.getProxy(customerService.getByLogin(user)).getAuthorities());
            } else {
                throw new AuthorizationException();
            }
        }
        return null;
    }

    @Override
    public Integer getAuthCustomerId() {
        String login = getAuthCustomerLogin();
        return customerService.getIdByLogin(login);
    }

    @Override
    public String getAuthCustomerLogin() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @Override
    public Customer getCustomerAuth() {
        return customerService.getByLogin(getAuthCustomerLogin());
    }


}
