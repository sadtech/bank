package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CustomerAdapter implements MapperJDBC<Customer>, SQLInsert<Customer> {

    public static final Logger log = Logger.getLogger(CustomerAdapter.class);
    public static final String CUSTOMER_ROLE = "customer_role";

    public List<Customer> getAll(ResultSet resultSet) {
        Map<Integer, Customer> customers = new HashMap<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    if (!customers.containsKey(resultSet.getInt("id"))) {
                        Customer customer = getCustomer(resultSet);
                        customers.put(customer.getId(), customer);
                    } else {
                        Customer customer = customers.get(resultSet.getInt("id"));
                        customer.getRoles().add(CustomerRole.valueOf(resultSet.getString(CUSTOMER_ROLE).toUpperCase()));
                    }
                }
            } catch (SQLException e) {
                log.error(e);
            }

        }
        return new ArrayList<>(customers.values());
    }

    private Customer getCustomer(ResultSet resultSet) throws SQLException {
        Customer customer = new Customer();
        customer.setId(resultSet.getInt("id"));
        customer.setUserName(resultSet.getString("login"));
        customer.setPassword(resultSet.getString("pass"));
        customer.setFirstName(resultSet.getString("first_name"));
        customer.setLastName(resultSet.getString("last_name"));
        if (resultSet.getString(CUSTOMER_ROLE) != null) {
            customer.setRoles(new HashSet<>(Collections.singleton(CustomerRole.valueOf(resultSet.getString(CUSTOMER_ROLE).toUpperCase()))));
        } else {
            customer.setRoles(new HashSet<>());
        }
        return customer;
    }

    @Override
    public Customer getOne(ResultSet resultSet) {
        List<Customer> customers = getAll(resultSet);
        if (!customers.isEmpty()) {
            return customers.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Map<String, String> getNameAndValues(Customer customer) {
        Map<String, String> nameAndValue = new TreeMap<>();
        nameAndValue.put("first_name", customer.getFirstName() != null ? "'" + customer.getFirstName() + "'" : null);
        nameAndValue.put("last_name", customer.getLastName() != null ? "'" + customer.getLastName() + "'" : null);
        nameAndValue.put("pass", customer.getPassword() != null ? "'" + customer.getPassword() + "'" : null);
        nameAndValue.put("login", customer.getUserName() != null ? "'" + customer.getUserName() + "'" : null);
        return nameAndValue;
    }

    @Override
    public Set<List<String>> getValues(Set<Customer> objects) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (Customer object : objects) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }

}
