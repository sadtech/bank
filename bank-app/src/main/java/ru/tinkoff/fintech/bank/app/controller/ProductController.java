package ru.tinkoff.fintech.bank.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.product.Product;
import ru.tinkoff.fintech.bank.core.exception.AccessDeniedException;
import ru.tinkoff.fintech.bank.core.service.auth.AuthService;
import ru.tinkoff.fintech.bank.app.service.TokenAuthenticationService;
import ru.tinkoff.fintech.bank.core.service.product.ProductService;
import ru.tinkoff.fintech.bank.app.utils.ResponseEntityGson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1")
@PreAuthorize("hasAnyAuthority('USER')")
public class ProductController {

    private final AuthService authService;
    private final Map<TypeProduct, ProductService> productServiceMap;

    public ProductController(TokenAuthenticationService authService, Map<TypeProduct, ProductService> productServiceMap) {
        this.authService = authService;
        this.productServiceMap = productServiceMap;
    }

    @GetMapping("customer/{customerId}/product")
    public ResponseEntity<String> getAllProduct(@PathVariable Integer customerId) {
        List<Product> products = new ArrayList<>();
        Customer customerAuth = authService.getCustomerAuth();
        if (customerAuth.getId().equals(customerId) || customerAuth.getRoles().contains(CustomerRole.ADMIN)) {
            for (ProductService productService : productServiceMap.values()) {
                products.addAll(productService.getProductsByCustomer(customerId));
            }
            return ResponseEntityGson.getJson(products, HttpStatus.OK);
        } else {
            return ResponseEntityGson.getJson(new AccessDeniedException(), HttpStatus.LOCKED);
        }
    }
}
