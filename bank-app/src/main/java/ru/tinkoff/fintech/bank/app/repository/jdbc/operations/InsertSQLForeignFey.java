package ru.tinkoff.fintech.bank.app.repository.jdbc.operations;

import java.util.List;
import java.util.Set;

public interface InsertSQLForeignFey<T, F> {

    Set<List<String>> getValues(T generalObject, F foreignOject);

}
