package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationLoan;

import javax.validation.constraints.NotNull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ApplicationLoanAdapter implements SQLInsert<ApplicationLoan>, MapperJDBC<ApplicationLoan> {

    public static final Logger log = Logger.getLogger(ApplicationLoanAdapter.class);

    @Override
    public Collection<ApplicationLoan> getAll(@NotNull ResultSet resultSet) {
        List<ApplicationLoan> applicationLoans = new ArrayList<>();
        try {
            while (resultSet.next()) {
                ApplicationLoan applicationLoan = getApplicationLoan(resultSet);
                applicationLoans.add(applicationLoan);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return applicationLoans;
    }

    @Override
    public ApplicationLoan getOne(@NotNull ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return getApplicationLoan(resultSet);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    private ApplicationLoan getApplicationLoan(@NotNull ResultSet resultSet) throws SQLException {
        ApplicationLoan applicationLoan = new ApplicationLoan();
        applicationLoan.setId(resultSet.getInt("id"));
        applicationLoan.setApplicationId(resultSet.getInt("application_id"));
        applicationLoan.setLimit(resultSet.getInt("limit_money"));
        applicationLoan.setTerm(resultSet.getInt("term"));
        return applicationLoan;
    }

    @Override
    public Map<String, String> getNameAndValues(ApplicationLoan applicationLoan) {
        Map<String, String> map = new TreeMap<>();
        map.put("application_id", applicationLoan.getApplicationId().toString());
        map.put("limit_money", applicationLoan.getLimit().toString());
        map.put("term", applicationLoan.getTerm().toString());
        return map;
    }

    @Override
    public Set<List<String>> getValues(Set<ApplicationLoan> objects) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (ApplicationLoan object : objects) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }
}
