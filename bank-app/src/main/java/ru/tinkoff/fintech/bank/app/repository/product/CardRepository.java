package ru.tinkoff.fintech.bank.app.repository.product;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.CardAdapter;
import ru.tinkoff.fintech.bank.core.domain.product.Card;

@Service
public class CardRepository extends AbstractProductRepositoryJDBC<Card> {

    public final CardAdapter cardAdapter = new CardAdapter();

    public CardRepository(JDBCService jdbcService) {
        super(jdbcService, "card");
        super.mapperJDBC = cardAdapter;
        super.sqlInsert = cardAdapter;
    }
}
