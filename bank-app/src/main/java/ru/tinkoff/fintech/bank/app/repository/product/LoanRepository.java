package ru.tinkoff.fintech.bank.app.repository.product;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.LoanAdapter;
import ru.tinkoff.fintech.bank.core.domain.product.Loan;

@Service
public class LoanRepository extends AbstractProductRepositoryJDBC<Loan> {

    public final LoanAdapter loanAdapter = new LoanAdapter();

    public LoanRepository(JDBCService jdbcService) {
        super(jdbcService, "loan");
        super.sqlInsert = loanAdapter;
        super.mapperJDBC = loanAdapter;
    }
}
