package ru.tinkoff.fintech.bank.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.domain.enums.CustomerRole;
import ru.tinkoff.fintech.bank.core.exception.AccessDeniedException;
import ru.tinkoff.fintech.bank.core.service.CustomerService;
import ru.tinkoff.fintech.bank.core.service.auth.AuthService;
import ru.tinkoff.fintech.bank.app.utils.ResponseEntityGson;

@RestController
@RequestMapping("api/v1/customer")
@PreAuthorize("hasAnyAuthority('USER')")
public class CustomerController {

    private final CustomerService customerService;
    private final AuthService authService;

    public CustomerController(CustomerService customerService, AuthService authService) {
        this.customerService = customerService;
        this.authService = authService;
    }

    @GetMapping("{customerId}")
    public ResponseEntity<String> getCustomerInfo(@PathVariable Integer customerId) {
        Customer customerAuth = authService.getCustomerAuth();
        if (customerAuth.getId().equals(customerId) || customerAuth.getRoles().contains(CustomerRole.ADMIN)) {
            return ResponseEntityGson.getJson(customerService.getByLogin(authService.getAuthCustomerLogin()), HttpStatus.OK);
        } else {
            return ResponseEntityGson.getJson(new AccessDeniedException(), HttpStatus.LOCKED);
        }

    }


}
