package ru.tinkoff.fintech.bank.app.repository.jdbc.operations;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SQLInsert<T> {

    Map<String, String> getNameAndValues(T object);

    Set<List<String>> getValues(Set<T> objects);

}
