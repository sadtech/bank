package ru.tinkoff.fintech.bank.app.repository.application;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.ApplicationAdapter;
import ru.tinkoff.fintech.bank.core.domain.application.Application;
import ru.tinkoff.fintech.bank.core.repository.ApplicationRepository;

import java.util.Collection;

@Service
public class ApplicationRepositoryJDBC implements ApplicationRepository {

    private final JDBCService jdbcService;
    private final ApplicationAdapter applicationAdapter = new ApplicationAdapter();

    public ApplicationRepositoryJDBC(JDBCService jdbcService) {
        this.jdbcService = jdbcService;
    }

    @Override
    public Integer add(Application application) {
        return jdbcService.insertAndGetId("application", applicationAdapter.getNameAndValues(application));
    }

    @Override
    public Application getById(Integer id) {
        String sql = "SELECT * FROM application WHERE id = " + id;
        return (Application) jdbcService.getOneResult(sql, applicationAdapter);
    }

    @Override
    public Collection<Application> getAll() {
        String sql = "SELECT * FROM application";
        return jdbcService.getResult(sql, applicationAdapter);
    }

    @Override
    public Collection<Application> getAllByCustomerId(Integer idCustomer) {
        String sql = "SELECT * FROM application WHERE customerId = " + idCustomer;
        return jdbcService.getResult(sql, applicationAdapter);
    }

    @Override
    public boolean edit(Application application) {
        return jdbcService.update("application", "id", applicationAdapter.getNameAndValues(application));
    }
}
