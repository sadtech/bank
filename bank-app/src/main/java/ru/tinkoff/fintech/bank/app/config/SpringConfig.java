package ru.tinkoff.fintech.bank.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.service.application.ApplicationFormService;
import ru.tinkoff.fintech.bank.core.service.product.ProductService;
import ru.tinkoff.fintech.bank.core.service.valid.ValidService;

import java.util.EnumMap;
import java.util.Map;

@Configuration
public class SpringConfig {

    @Bean
    public Map<TypeProduct, ApplicationFormService> productFormServiceMap(ApplicationFormService... applicationFormServices) {
        Map<TypeProduct, ApplicationFormService> productFormServiceMap = new EnumMap<>(TypeProduct.class);
        for (ApplicationFormService applicationFormService : applicationFormServices) {
            productFormServiceMap.put(applicationFormService.getTypeProductService(), applicationFormService);
        }
        return productFormServiceMap;
    }

    @Bean
    public Map<TypeProduct, ValidService> productValidServiceMap(ValidService... validServices) {
        Map<TypeProduct, ValidService> validServiceMap = new EnumMap<>(TypeProduct.class);
        for (ValidService validService : validServices) {
            validServiceMap.put(validService.getTypeProductValidate(), validService);
        }
        return validServiceMap;
    }

    @Bean
    public Map<TypeProduct, ProductService> productServiceMap(ProductService... productServices) {
        Map<TypeProduct, ProductService> productServiceMap = new EnumMap<>(TypeProduct.class);
        for (ProductService productService : productServices) {
            productServiceMap.put(productService.getTypeProductService(), productService);
        }
        return productServiceMap;
    }

}
