package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.product.Card;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CardAdapter implements SQLInsert<Card>, MapperJDBC<Card> {

    public static final Logger log = Logger.getLogger(CardAdapter.class);

    @Override
    public Collection<Card> getAll(ResultSet resultSet) {
        List<Card> applicationLoans = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Card card = getCard(resultSet);
                applicationLoans.add(card);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return applicationLoans;
    }

    @Override
    public Card getOne(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return getCard(resultSet);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    private Card getCard(ResultSet resultSet) throws SQLException {
        Card card = new Card();
        card.setId(resultSet.getInt("id"));
        card.setNumber(resultSet.getInt("number"));
        card.setSecretKey(resultSet.getInt("secret_key"));
        card.setMouth(resultSet.getInt("mouth"));
        card.setYear(resultSet.getInt("year"));
        card.setCustomerId(resultSet.getInt("customer_id"));
        return card;
    }

    @Override
    public Map<String, String> getNameAndValues(Card card) {
        Map<String, String> map = new TreeMap<>();
        map.put("mouth", card.getMouth().toString());
        map.put("number", card.getNumber().toString());
        map.put("secret_key", card.getSecretKey().toString());
        map.put("year", card.getYear().toString());
        map.put("customer_id", card.getCustomerId().toString());
        return map;
    }

    @Override
    public Set<List<String>> getValues(Set<Card> cards) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (Card object : cards) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }
}
