package ru.tinkoff.fintech.bank.app.repository.jdbc;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface JDBCService {

    Collection getResult(String sql, MapperJDBC mapperJDBC);

    Object getOneResult(String sql, MapperJDBC mapperJDBC);

    Integer insert(String tableName, Map<String, String> nameAndValue);

    Integer insertAndGetId(String tableName, Map<String, String> nameAndValue);

    Integer insert(String tableName, List<String> fieldNames, Set<List<String>> valuesNameAndValue);

    boolean update(String tableName, String whereRow, Map<String, String> nameRowAndValue);

    void delete(String tableName, String whereValue);
}
