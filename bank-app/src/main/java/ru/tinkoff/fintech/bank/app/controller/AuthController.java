package ru.tinkoff.fintech.bank.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.bank.core.domain.Customer;
import ru.tinkoff.fintech.bank.core.exception.ApiException;
import ru.tinkoff.fintech.bank.core.exception.AuthorizationException;
import ru.tinkoff.fintech.bank.core.service.CustomerService;
import ru.tinkoff.fintech.bank.app.service.TokenAuthenticationService;
import ru.tinkoff.fintech.bank.app.utils.ResponseEntityGson;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final CustomerService customerService;

    public AuthController(AuthenticationManager authenticationManager, CustomerService customerService) {
        this.authenticationManager = authenticationManager;
        this.customerService = customerService;
    }

    @PostMapping("login")
    @ResponseBody
    public ResponseEntity<String> generateToken(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (customerService.checkByLogin(username)) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);
            try {
                if (authenticationManager.authenticate(authentication).isAuthenticated()) {
                    return ResponseEntityGson.getJson(TokenAuthenticationService.addAuthentication(username), HttpStatus.CREATED);
                } else {
                    return ResponseEntityGson.getJson(new AuthorizationException(), HttpStatus.BAD_REQUEST);
                }
            } catch (AuthenticationException exception) {
                return ResponseEntityGson.getJson(new AuthorizationException(), HttpStatus.BAD_REQUEST);
            }
        } else {
            return ResponseEntityGson.getJson(new AuthorizationException(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("registration")
    public ResponseEntity<String> registration(@RequestBody Customer customer) {
        try {
            return ResponseEntityGson.getJson(customerService.registration(customer), HttpStatus.CREATED);
        } catch (ApiException e) {
            return ResponseEntityGson.getJson(e, HttpStatus.BAD_REQUEST);
        }
    }

}
