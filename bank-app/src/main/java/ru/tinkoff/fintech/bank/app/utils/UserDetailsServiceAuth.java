package ru.tinkoff.fintech.bank.app.utils;

import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.tinkoff.fintech.bank.core.service.CustomerService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class UserDetailsServiceAuth {

    public static final Logger log = Logger.getLogger(UserDetailsServiceAuth.class);

    private UserDetailsServiceAuth() {
        throw new IllegalStateException();
    }

    public static UserDetailsService getProxy(CustomerService customerService) {
        InvocationHandler invocationHandler = new MyUserDetailsService(customerService);
        Class[] classes = new Class[]{UserDetailsService.class};
        return (UserDetailsService) Proxy.newProxyInstance(UserDetailsServiceAuth.class.getClassLoader(), classes, invocationHandler);
    }

    static class MyUserDetailsService implements UserDetailsService, InvocationHandler {

        private CustomerService customerService;

        MyUserDetailsService(CustomerService customerService) {
            this.customerService = customerService;
        }

        @Override
        public Object invoke(Object o, Method method, Object[] objects) {
            try {
                return method.invoke(this, objects);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error(e);
            }
            return null;
        }

        @Override
        public UserDetails loadUserByUsername(String s) {
            return UserDetailsAuth.getProxy(customerService.getByLogin(s));
        }
    }
}
