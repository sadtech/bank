package ru.tinkoff.fintech.bank.app.repository.application;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.ApplicationCreditCardAdapter;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationCreditCard;

@Service
public class ApplicationCreditCardRepository extends AbstractApplicationFormRepositoryJDBC<ApplicationCreditCard> {

    private final ApplicationCreditCardAdapter applicationCreditCardAdapter = new ApplicationCreditCardAdapter();

    protected ApplicationCreditCardRepository(JDBCService jdbcService) {
        super(jdbcService, "application_credit_card");
        super.sqlInsert = applicationCreditCardAdapter;
        super.mapperJDBC = applicationCreditCardAdapter;
    }
}
