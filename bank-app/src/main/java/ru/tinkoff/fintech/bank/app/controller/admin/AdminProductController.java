package ru.tinkoff.fintech.bank.app.controller.admin;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.bank.core.domain.enums.TypeProduct;
import ru.tinkoff.fintech.bank.core.domain.product.Product;
import ru.tinkoff.fintech.bank.core.service.product.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/product")
@PreAuthorize("hasAnyAuthority('ADMIN')")
public class AdminProductController {

    private final Map<TypeProduct, ProductService> productServiceMap;

    public AdminProductController(Map<TypeProduct, ProductService> productServiceMap) {
        this.productServiceMap = productServiceMap;
    }

    @GetMapping("customer/{customerId}")
    public ResponseEntity<List<Product>> getAllProduct(@PathVariable Integer customerId) {
        List<Product> productsCustomer = new ArrayList<>();
        for (ProductService productService : productServiceMap.values()) {
            productsCustomer.addAll(productService.getProductsByCustomer(customerId));
        }
        return new ResponseEntity<>(productsCustomer, HttpStatus.OK);
    }

    @GetMapping("type/{type}")
    public ResponseEntity<List<Product>> getAllProductByType(@PathVariable TypeProduct type) {
        return new ResponseEntity<>(productServiceMap.get(type).getAll(), HttpStatus.OK);
    }

}
