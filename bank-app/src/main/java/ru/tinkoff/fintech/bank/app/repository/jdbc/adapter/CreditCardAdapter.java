package ru.tinkoff.fintech.bank.app.repository.jdbc.adapter;

import org.apache.log4j.Logger;
import ru.tinkoff.fintech.bank.app.repository.jdbc.MapperJDBC;
import ru.tinkoff.fintech.bank.app.repository.jdbc.operations.SQLInsert;
import ru.tinkoff.fintech.bank.core.domain.product.CreditCard;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CreditCardAdapter implements SQLInsert<CreditCard>, MapperJDBC<CreditCard> {

    public static final Logger log = Logger.getLogger(CreditCardAdapter.class);

    @Override
    public Collection<CreditCard> getAll(ResultSet resultSet) {
        List<CreditCard> applicationLoans = new ArrayList<>();
        try {
            while (resultSet.next()) {
                CreditCard creditCard = getCreditCard(resultSet);
                applicationLoans.add(creditCard);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return applicationLoans;
    }

    @Override
    public CreditCard getOne(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return getCreditCard(resultSet);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return null;
    }

    private CreditCard getCreditCard(ResultSet resultSet) throws SQLException {
        CreditCard creditCard = new CreditCard();
        creditCard.setId(resultSet.getInt("id"));
        creditCard.setNumber(resultSet.getInt("number"));
        creditCard.setSecretKey(resultSet.getInt("secret_key"));
        creditCard.setMouth(resultSet.getInt("mouth"));
        creditCard.setYear(resultSet.getInt("year"));
        creditCard.setLimit(resultSet.getInt("limit_money"));
        creditCard.setCustomerId(resultSet.getInt("customer_id"));
        return creditCard;
    }

    @Override
    public Map<String, String> getNameAndValues(CreditCard creditCard) {
        Map<String, String> map = new TreeMap<>();
        if (creditCard.getId() != null) {
            map.put("id", creditCard.getId().toString());
        }
        map.put("limit_money", creditCard.getLimit().toString());
        map.put("number", creditCard.getNumber().toString());
        map.put("secret_key", creditCard.getSecretKey().toString());
        map.put("mouth", creditCard.getMouth().toString());
        map.put("year", creditCard.getYear().toString());
        map.put("customer_id", creditCard.getCustomerId().toString());
        return map;
    }

    @Override
    public Set<List<String>> getValues(Set<CreditCard> objects) {
        Set<List<String>> namesAndValues = new HashSet<>();
        for (CreditCard object : objects) {
            namesAndValues.addAll(Collections.singleton(new ArrayList<>(getNameAndValues(object).values())));
        }
        return namesAndValues;
    }
}
