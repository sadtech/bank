package ru.tinkoff.fintech.bank.app.repository.application;

import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.bank.app.repository.jdbc.JDBCService;
import ru.tinkoff.fintech.bank.app.repository.jdbc.adapter.ApplicationCardAdapter;
import ru.tinkoff.fintech.bank.core.domain.application.ApplicationCard;

@Service
public class ApplicationCardRepository extends AbstractApplicationFormRepositoryJDBC<ApplicationCard> {

    public static final ApplicationCardAdapter applicationCardAdapter = new ApplicationCardAdapter();

    protected ApplicationCardRepository(JDBCService jdbcService) {
        super(jdbcService, "application_card");
        super.sqlInsert = applicationCardAdapter;
        super.mapperJDBC = applicationCardAdapter;
    }

}
